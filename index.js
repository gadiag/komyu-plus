  // navbar
  const navEl = document.querySelector('.navbar');

  window.addEventListener('scroll', () => {
    if (window.scrollY >= 56) {
      navEl.classList.add('navbar-scrolled');
    }else if (window.scrollY < 56) {
      navEl.classList.remove('navbar-scrolled');
    }
  })



  const cards = document.querySelectorAll('.card-header');

cards.forEach(card => {
  const target = parseInt(card.textContent);
  let current = 0;
  
  const increment = setInterval(() => {
    card.textContent = `${++current}%`;
    if (current === target) clearInterval(increment);
  }, 30);
});
